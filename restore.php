<?php
# backup.php
# Restore a backup.
# All this does, really, is dump the contents of the backup file, if it
# exists.

header("Content-type: text/plain; charset=utf-8");

# Collect backup files going back up to a year.
$t = new DateTime("-1 year");
for ($i = 0; $i < 13; $i++)
{
	$BACKUPFILE = "backup/backup." . $t->format("Y-m");
	if (file_exists($BACKUPFILE))
	{
		if (is_readable($BACKUPFILE))
			readfile($BACKUPFILE);
		else {
			# XXX - Perhaps ought to collect all backup files
			# and make sure they're all readable before
			# readfile()ing any of them.
			header("HTTP/1.0 400 Can't read backup file");
			exit(0);
		}
	}
	$t->add(new DateInterval("P1M"));
}
?>
