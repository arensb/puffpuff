var pages;		// List of pages

var smoke_button;
var history_chart;
var ctx;		// Graphic context for history_chart
var current_graph;	// Function to draw whichever graph we're
			// currently showing

var hist;		// Historical events and totals.

/* graph_style
 * A container for all of the various style settings for the graphs.
 */
var graph_style = {
	hour_bar_color:	"rgb(200, 200, 200)",
			// Even-numbered hour lines
	count_line_color:	"rgb(200, 128, 128)",
			// Horizontal count lines
	line_chart_color:	"rgb(0, 128, 0)",
			// Color of graph line in line graphs (day)
	line_width:	2,
			// Width of graph line in line graphs (day)
	line_point_color:	"rgb(0, 0, 0)",

	day_bar_color:	"rgb(128, 128, 128)",
			// Color of day-separator lines
	bar_bg:		"rgb(128, 128, 255)",
			// Background color of bars in bar chart
	bar_outline_rolor:	"rgb(0, 0, 0)",
			// Outline of bars in bar chart
	label_color:	"rgb(0, 0, 0)",
			// Color of axis labels
};

document.addEventListener("DOMContentLoaded", init, false);
window.addEventListener("load",
			  function() {
				  // Try to hide the URL bar
				  window.scrollTo(0, 1);
			  }, false);

/* History
 * Class for smoking history. Keeps individual events for the current
 * day, daily totals for the last 30 days, and monthly totals for the
 * last 12 months.
 */
function History(obj)
{
	function parsedate(date)
	{
		/* Ugly hack: JavaScript in the Android browser can't
		 * parse dates of the form "YYYY-MM-DDThh:mm:ss.000Z".
		 * Unfortunately, that's how JSON.stringify(Date)
		 * writes it. So we have this function that parses out
		 * the numbers and passes them to the many-arguments
		 * form of the Date constructor.
		 *
		 * Unfortunately (again), "new Date" assumes its
		 * arguments refer to local time, whereas
		 * JSON.stringify(Date) stores them in UTC. So we
		 * first use Date.UTC() to convert parsed numbers to
		 * milliseconds since the epoch, then use that to
		 * construct a Date.
		 *
		 * Ugh.
		 */
		var m;
		if (m = date.match(/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})\.(\d{3})Z$/))
			return new Date(Date.UTC(m[1], m[2]-1, m[3], m[4], m[5], m[6], m[7]));
		else
			return date;
	}

	if (obj != undefined)
	{
		/* We have a template. Copy it. */
		for (key in obj)
			this[key] = obj[key];

		/* Convert strings to dates where needed */
		// Don't touch today_s, because it's a hash index
		// string, not a Date object.
		for (var i = 0, len = this.today.length; i < len; i++)
			this.today[i] = parsedate(this.today[i]);

		for (var i = 0, len = this.unsaved.length; i < len; i++)
			this.unsaved[i] = parsedate(this.unsaved[i]);
		return;
	}

	/* Create a new object from scratch */
	this.today_s = null;		// Today as a string
	this.today = new Array();	// Daily events
	this.days = {};	// Daily totals
	this.months = {};	// Monthly totals

	this.unsaved = new Array();	// Events that haven't been
					// backed up yet.
}

/* History.add(t)
 * Add an event with time t.
 */
History.prototype.add = function(t)
{
	/* A note on time offsets and such: while the time recorded is
	 * in UTC, the totals are intended to reflect a more
	 * conceptual notion of "day", i.e. whatever day/month it is
	 * in whichever timezone was current when you lit up. So if
	 * you travel west, you might experience May 10 for more than
	 * 24 hours, and all events that occur on May 10 local time
	 * will go in the "May 10" and "May" slots.
	 */
	//var t_sec  = t.getSeconds();
	//var t_min  = t.getMinutes();
	//var t_hour = t.getHours();
	var t_day  = t.getDate();
	var t_mon  = t.getMonth()+1;	// Months are 0-based
	var t_year = t.getFullYear();
	var t_mon_key = t_year + "-" +
		(t_mon < 10 ? "0" : "") + t_mon;
	var t_day_key = t_mon_key + "-" +
		(t_day < 10 ? "0" : "") + t_day;

	// Add t to the events that haven't been backed up yet.
	this.unsaved.push(t);

	var now = new Date();		// When is now?

	/* today */
	this.today.push(t);

	/* Daily totals */
	// Add the event to today's total.
	if (this.days[t_day_key] == undefined)
	{
		this.days[t_day_key] = 0;
	}
	this.days[t_day_key]++;

	/* Monthly totals */
	// Add the event to this month's total.
	if (this.months[t_mon_key] == undefined)
	{
		this.months[t_mon_key] = 0;
	}
	this.months[t_mon_key]++;

	/* Save to localStorage */
	localStorage.setItem('history', JSON.stringify(this));
}

/* clean
 * Remove old entries.
 */
History.prototype.cleanup = function()
{
	var now = new Date();		// How soon is now?
	var now_mon =now.getFullYear() + "-" +
		(now.getMonth() < 9 ? "0" : "") +	// Months are 0-based
		(now.getMonth()+1);	// Parens to make + be
					// addition, not
					// concatenation.
	var now_date = now_mon + "-" +
		(now.getDate() < 10 ? "0" : "") +
		now.getDate();

	var midnight = now;		// Previous midnight.
	midnight.setHours(0, 0, 0, 0);

	var last_month = now;		// Cutoff point for 30 days ago.
	last_month.setDate(last_month.getDate()-30);

	var last_year = now;		// Cutoff point for 1 year ago.
	last_year.setFullYear(last_year.getFullYear()-1);

	/* Today */
	if (this.today_s != now_date)
	{
		// Start a new day
		this.today = new Array();
		this.today_s = now_date;
	}

	// Remove entries in 'today' that came before midnight.
	while (this.today[0] != null &&
	       this.today[0].getTime() < midnight.getTime())
		this.today.shift();

	/* Daily totals */
	// Clear out entries older than 30 days
	for (key in this.days)
	{
		var matches = key.match(/^(\d+)-(\d+)-(\d+)/);
				// YYYY-MM-DD
		var tmp_t = new Date(matches[1],
				     matches[2]-1,	// Months are 0-based
				     matches[3]);
		// Is tmp_t less than 30 days ago?
		if (now.getTime() - tmp_t.getTime() <= 30 * 86400 * 1000)
			continue;

		// No. Remove this entry
		delete(this.days[key]);
	}

	/* Monthly totals */
	// Clear out entries older than a year.
	for (key in this.months)
	{
		var matches = key.match(/^(\d+)-(\d+)/);
				// YYYY-MM
		var tmp_t = new Date(matches[1],
				     matches[2]-1);	// Months are 0-based
		// Is tmp_t less than 367 days ago? (366 days in leap
		// years, and an extra day on general principle)
		if (now.getTime() - tmp_t.getTime() <= 367 * 86400 * 1000)
			continue;

		// No. Remove this entry
		delete(this.months[key]);
	}
}

History.prototype.daily_totals = function()
{
	var totals = new Array();
	var t = new Date();
	var key = t.getFullYear() + "-" +
		(t.getMonth() < 9 ? "0" : "") + (t.getMonth()+1) + "-" +
		(t.getDate() < 10 ? "0" : "") + t.getDate();
	if (this.days[key] == null)
		totals.push(0)
	else
		totals.push(this.days[key]);

	for (var i = 0; i < 30; i++)
	{
		t.setDate(t.getDate()-1);

		key = t.getFullYear() + "-" +
			(t.getMonth() < 9 ? "0" : "") + (t.getMonth()+1) + "-" +
			(t.getDate() < 10 ? "0" : "") + t.getDate();
		if (this.days[key] == null)
			totals.push(0)
		else
			totals.push(this.days[key]);
	}
	return totals.reverse();
}

History.prototype.monthly_totals = function()
{
	var totals = new Array();
	var t = new Date();
	var key = t.getFullYear() + "-" +
		(t.getMonth() < 9 ? "0" : "") + (t.getMonth()+1);

	if (this.months[key] == null)
		totals.push(0);
	else
		totals.push(this.months[key]);

	for (var i = 0; i < 12; i++)
	{
		t.setMonth(t.getMonth()-1);

		key = t.getFullYear() + "-" +
			(t.getMonth() < 9 ? "0" : "") + (t.getMonth()+1);
		if (this.months[key] == null)
			totals.push(0);
		else
			totals.push(this.months[key]);
	}
	return totals.reverse();
}

/* backup()
 * Do a backup of whatever hasn't been backed up yet.
 */
History.prototype.backup = function(interactive)
{
	if (this.unsaved.length == 0)
		// Nothing to save
		return;

	// Send unsaved to backup.php
	var request = new XMLHttpRequest();
			// XXX - Error-checking.
	var url = "backup.php";

	request.open('POST', url, true);
	request.setRequestHeader('Content-Type',
		'application/x-www-form-urlencoded');

	request.onreadystatechange = function() {
		hist._backup_cb(this, interactive)
		};

	// Get list of events, and convert to a string.
	var params = "events=";
	for (var i = 0, len = this.unsaved.length; i < len; i++)
	{
		if (i != 0)
			params += "\n";
		// Convert Date to time_t: divide by 1000 (to convert
		// milliseconds to seconds) and round down.
		params += Math.floor(this.unsaved[i].getTime()/1000);
	}

	request.setRequestHeader("Content-length", params.length);
	request.setRequestHeader("Connection", "close");	// XXX - What does this do?
	request.send(params);
	// XXX - Error-checking
}

/* _backup_cb
 * Callback function for backup().
 */
History.prototype._backup_cb = function(req, interactive)
{
	/* Wait until we have all the data */
	if (req.readyState != 4)
		return;

	// If it worked, zero out this.unsaved.
	if (req.status == "200")
	{
		if (interactive)
			alert("Backup successful");
		this.unsaved = new Array();
	} else {
		if (interactive)
			alert("Backup failed: "+req.status+" "+req.statusText);
	}
}

/* restore
 * Restore data from backup.
 */
History.prototype.restore = function()
{
	// Send request to restore.php
	var request = new XMLHttpRequest();
	var url = "restore.php";

	request.open('POST', url, true);
		// WTF? For some reason, Chrome (and possibly Safari)
		// doesn't like 'GET': it sets request.status to an
		// error, without even the courtesy of throwing an
		// exception.
	request.onreadystatechange = function() { hist._restore_cb(this) };
	request.send();
}

History.prototype._restore_cb = function(req)
{
	/* Wait until we have all the data */
	if (req.readyState != 4)
		return;

	// Check HTTP status
	if (req.status != "200")
	{
		alert("Restore failed: "+req.status+" "+req.statusText);
		return;
	}

	// Split by lines.
	var lines = req.responseText.split("\n");

	// XXX - Make sure the data looks sane?

	// Clear the old data
	// This is just copied from the constructor.
	this.today_s = null;
	this.today   = new Array();
	this.days    = {};
	this.months  = {};

	/* Add the events */

	// The elegant approach would be to call this.add() a bunch of
	// times, but that takes way too long. So instead, we
	// brute-force it.

	var now = new Date();
	var midnight = new Date(now).setHours(0, 0, 0, 0);
	var month_ago = new Date(now).setDate(now.getDate()-30, 0, 0, 0, 0);
	var year_ago = new Date(now).setFullYear(now.getFullYear()-1);
	for (var i = 0, len = lines.length; i < len; i++)
	{
		var t = new Date(lines[i]*1000);
		if (t < year_ago)
		{
			// Ignore really old events
			continue;
		}
		var t_mon_key = t.getFullYear() + "-" +
			(t.getMonth() < 9 ? "0" : "") + (t.getMonth()+1);

		if (this.months[t_mon_key] == null)
			this.months[t_mon_key] = 0;
		this.months[t_mon_key]++;

		if (t < month_ago)
			continue;

		var t_day_key = t_mon_key + "-" +
			(t.getDate() < 10 ? "0" : "") + t.getDate();

		if (this.days[t_day_key] == null)
			this.days[t_day_key] = 0;
		this.days[t_day_key]++;

		if (t < midnight)
			continue;
		this.today.push(t);
		this.today_s = t_day_key;
	}
	this.cleanup();

	this.unsaved = new Array();	// It's a fresh restore.
					// Nothing is unsaved.

	/* Save to localStorage */
	localStorage.setItem('history', JSON.stringify(this));

	draw_day_graph();
	alert("Restore finished");
}

/* init
 * Initialize everything when the page is loaded.
 */
function init()
{
	/* Initialize history */
	// Try getting it from local storage
	var hist_str = localStorage.getItem('history');
	if (hist_str == null)
	{
		// Nope. Start a new history
		hist = new History();
	} else {
		hist = new History(JSON.parse(hist_str));

		// Do a (non-interactive) backup
		hist.backup(false);
		hist.cleanup();
	}

	/* Find elements in DOM; bind them to appropriate functions. */

	pages = document.getElementsByClassName("page");

	smoke_button  = document.getElementById("smoke");
	smoke_button.addEventListener('click', do_smoke, false);

	history_chart = document.getElementById("history-chart");
	ctx = history_chart.getContext("2d");

	/* Initialize text-drawing stuff */
	CanvasTextFunctions.enable(ctx);


	/* Links to pages we can show */
	var show_page_main = document.getElementById("show-page-main");
	show_page_main.addEventListener("click",
					function(ev) {
						flip_to_page("main");
					});

	var show_page_graph = document.getElementById("show-page-graph");
	show_page_graph.addEventListener("click",
					function(ev) {
						flip_to_page("graph");
					});

	var show_page_edit = document.getElementById("show-page-edit");
	show_page_edit.addEventListener("click",
					function(ev) {
						flip_to_page("edit");
					});

	var show_page_conf = document.getElementById("show-page-conf");
	show_page_conf.addEventListener("click",
					function(ev) {
						flip_to_page("conf");
					});

	/* Links to graphs we can show */
	var show_day_chart   = document.getElementById("show-day");
	show_day_chart.addEventListener('click', draw_day_graph, false);

	var show_week_chart  = document.getElementById("show-week");
	show_week_chart.addEventListener('click', draw_week_graph, false);

	var show_30d_chart = document.getElementById("show-30d");
	show_30d_chart.addEventListener('click', draw_30d_graph, false);

	var show_12m_chart  = document.getElementById("show-12m");
	show_12m_chart.addEventListener('click', draw_12m_graph, false);

	/* Notice orientation change, so we can draw a big graph */
	window.onorientationchange = update_orientation;

	// Draw graph
	current_graph = draw_day_graph;
	current_graph();
}

/* do_smoke
 * Called when user clicks the "smoke" button.
 */
function do_smoke(ev)
{
	/* Add entry to database */
	hist.add(new Date());

	/* And show the new graph */
	draw_day_graph();
	flip_to_page("graph");
}

/* flip_to_page
 * Display the given page.
 */
function flip_to_page(name)
{
	for (var i = 0; i < pages.length; i++)
	{
		var p = pages[i];

		if (p.id == name+"-page")
			p.style.display = "block";
		else
			p.style.display = "none";
	}
}

/* draw_day_graph
 * Draw a graph of today's smoking pattern.
 */
function draw_day_graph()
{
	current_graph = draw_day_graph;
			// Remember which graph we're on

	var midnight = new Date();
	midnight.setHours(0, 0, 0, 0);
		// Most recent midnight: start of today
	var tomorrow = new Date(midnight);
	tomorrow.setDate(tomorrow.getDate()+1);
		// The following midnight

	/* Record the times on the left and right edges of the graph.
	 * Yeah, this looks like a hack, but it's useful.
	 */
	history_chart.left_time = midnight;
	history_chart.right_time = tomorrow;

	var times = hist.today;		// Times to plot

	var width = history_chart.width;
	var height = history_chart.height;

	// Figure out how tall the graph should be:
	// Make it big enough to hold today's data (+1). As a
	// guideline, use the maximum of the last 30 days' totals.
	var max_result = Math.max.apply(null, hist.daily_totals());
	if (times.length > max_result)
		max_result = times.length;
	// XXX - Round up to the nearest multiple of 5.
	var ticks = loose_label(3, 0, max_result+1);
	max_result = ticks[ticks.length-1];
			// Use the last tick for the top of the graph

	/* Clear old graph */
	ctx.clearRect(0, 0, width, height);

	/* Draw the grid */
	// Draw a vertical line for each even-numbered hour
	var hr_width = width / 24;
	var sec_width = width / 86400;
	ctx.beginPath();
	ctx.strokeStyle = graph_style.hour_bar_color;

	for (var i = 0; i <= 24; i += 2)
	{
		ctx.moveTo(i*hr_width, 0);
		ctx.lineTo(i*hr_width, height);
	}
	ctx.stroke();

	/* Hour labels */
	var descent = ctx.fontDescent("sans", 8);
	ctx.strokeStyle = "rgb(0, 0, 0)";
	for (var i = 0; i <= 24; i+= 2)
		ctx.drawText("sans", 8, i*hr_width, height - descent, i+"h");

	// Draw horizontal lines along the Y axis
	var y_scale = height / max_result;

	ctx.beginPath();
	ctx.strokeStyle = graph_style.count_line_color;
	for (var y = 0; y <= max_result+1; y++)
	{
		ctx.moveTo(0, height - y*y_scale);
		ctx.lineTo(width, height - y*y_scale);
	}
	ctx.stroke();

	// Draw lines every 5 count
	ctx.beginPath();
	ctx.strokeStyle = "rgb(100, 64, 64)";
	for (var i = 0, len = ticks.length; i < len; i++)
	{
		var y = ticks[i];
		ctx.moveTo(0, height - y*y_scale);
		ctx.lineTo(width, height - y*y_scale);
	}
	ctx.stroke();

	/* Count labels */
	ctx.strokeStyle = "rgb(0, 0, 0)";
	for (var y = 1; y <= max_result; y++)
	{
		ctx.drawText("sans", 8, 0, height - y*y_scale, y);
	}

	// Draw the graph
	ctx.beginPath();
	var old_lineWidth = ctx.lineWidth;
	ctx.lineWidth = graph_style.line_width;
	ctx.fillStyle   = graph_style.line_point_color;

	for (var i = 0, len = times.length; i < len; i++)
	{
		var x = (times[i].getTime()/1000 -
			 Math.floor(history_chart.left_time/1000)) *
			sec_width;
		var y = height - ((i+1) * y_scale);

		ctx.fillRect(x-2, y-2, 5, 5);
		if (i == 0)
			ctx.moveTo(x, y);
		else
			ctx.lineTo(x, y);
	}
	ctx.stroke();
	ctx.lineWidth = old_lineWidth;
}

/* draw_week_graph
 * Draw a graph of the last 7 days.
 */
function draw_week_graph()
{
	current_graph = draw_week_graph;
			// Remember which graph we're on

	var start_time = new Date();
	start_time.setHours(0, 0, 0, 0);
			// Most recent midnight
	start_time.setDate(start_time.getDate() - start_time.getDay());
			// Subtract the day of the week to get the
			// most recent Sunday at 00:00.
	var end_time = new Date(start_time);
	end_time.setDate(end_time.getDate()+7);
			// Add 7 days to get following Sunday at 00:00

	// Figure out times for left and right sides of graph
	history_chart.left_time = start_time;
	history_chart.right_time = end_time;

	// Get daily totals and draw graph
	draw_bar_graph(7, hist.daily_totals().reverse());
}

/* draw_30d_graph
 * Draw a graph of the last 30 days.
 */
function draw_30d_graph()
{
	current_graph = draw_30d_graph;
			// Remember which graph we're on

	var start_time = new Date();
	start_time.setHours(0, 0, 0, 0);
			// Most recent midnight
	start_time.setDate(start_time.getDate()-30);
			// 30 days ago
	var end_time = new Date();
	end_time.setDate(end_time.getDate()+1);
	end_time.setHours(0, 0, 0, 0);

	var num_days = Math.floor((end_time.getTime() - start_time.getTime()) / 86400000);
			// Number of days in this month

	// Figure out times for left and right sides of graph
	history_chart.left_time = start_time;
	history_chart.right_time = end_time;

	// Get daily totals and draw graph
	draw_bar_graph(30, hist.daily_totals().reverse());
}

/* draw_12m_graph
 * Draw a graph of the last 12 months.
 */
function draw_12m_graph()
{
	current_graph = draw_12m_graph;
			// Remember which graph we're on

	var start_time = new Date();
	start_time.setHours(0, 0, 0, 0);
			// Most recent midnight
	start_time.setDate(start_time.getDate()-365)
			// 365 days ago

	var end_time = new Date();
	end_time.setDate(end_time.getDate()+1);
			// The midnight after today
	end_time.setHours(0, 0, 0, 0);

	// Figure out times for left and right sides of graph
	history_chart.left_time = start_time;
	history_chart.right_time = end_time;

	// Get monthly totals and draw graph
	draw_bar_graph(12, hist.monthly_totals().reverse());
}

/* draw_bar_graph
 * Used by most of the draw_*_graph() functions: does the actual work
 * of drawing a bar graph.
 */
function draw_bar_graph(num_bars, results)
{
	var width = history_chart.width;
	var height = history_chart.height;

	/* Clear old graph */
	ctx.clearRect(0, 0, width, height);

	/* Draw the grid */
	// Draw a vertical line for each day
	var bar_width = width / num_bars;
	ctx.beginPath();
	ctx.strokeStyle = graph_style.day_bar_color;
	for (var i = 0; i <= num_bars; i++)
	{
		ctx.moveTo(i*bar_width, 0);
		ctx.lineTo(i*bar_width, height);
	}
	ctx.stroke();

	var num_results = results.length;
	var max_result = 0;

	// Find the largest 'tot' in 'results', i.e., the largest value
	// that must fit in the graph.
	for (var i = 0; i < num_results; i++)
		max_result = Math.max(max_result,
				      results[i]/*results.rows.item(i).tot*/);

	var ticks = loose_label(5, 0, max_result+1);
			// The +1 is there in case max_result is a round
			// number, to leave some space at the top of the
			// graph, to make it pretty.
	max_result = ticks[ticks.length-1];
			// Use the last tick for the top of the graph.
	var y_scale = height / max_result;
			// Number by which to multiply Y values (which
			// we express in convenient units) to get
			// pixels.
	// XXX - Might also want major and minor ticks.

	// XXX - Write labels along bottom?

	/* Draw the graph. */
	ctx.fillStyle = graph_style.bar_bg;
	ctx.strokeStyle = graph_style.bar_outline_color;
	for (var i = 0; i < num_results; i++)
	{
		var row = results[i]/*results.rows.item(i)*/;
		if (row == undefined)
			row = 0;

		// XXX - Here, we assume that the results are in
		// reverse chronological order (latest first).
		// This ought to be settable with an argument.
		ctx.fillRect((num_bars-i-1)*bar_width,
			     height - (row*y_scale),
			     bar_width,
			     row*y_scale);
		ctx.strokeRect((num_bars-i-1)*bar_width,
			       height - (row*y_scale),
			       bar_width,
			       row*y_scale);
	}

	// Draw horizontal grid lines
	ctx.beginPath();
	ctx.strokeStyle = graph_style.bar_outline_color;
	for (var i = 0; i < ticks.length; i++)
	{
		ctx.moveTo(0, height-(ticks[i]*y_scale));
		ctx.lineTo(width, height-(ticks[i]*y_scale));
	}
	ctx.stroke();

	// Draw labels on Y axis
	ctx.beginPath();
	ctx.strokeStyle = graph_style.label_color;
	for (var i = 1; i < ticks.length; i++)
	{
		ctx.drawText("sans", 8,
			     0,
			     height - (ticks[i]*y_scale) + 8,
			     ticks[i].toString());
	}
	ctx.stroke();

	// XXX - Draw rolling averages?
	// XXX - For the current day/month, perhaps extrapolate what
	// the total will be at the end of the month?
}

/* update_orientation
 * Change layout when the device is rotated.
 */
function update_orientation()
{
	// XXX - Ought to set a class on the body or something. Do
	// other resizing in CSS.

	// XXX - Sizes hardcoded for iPod Touch, so wrong for Android
	// etc.
	switch (window.orientation)
	{
	    case 90:	// Landscape, button on the right(?)
	    case -90:	// Landscape, button on the left(?)
		history_chart.width = 480;
		history_chart.height = 200;
		break;
	    case 0:	// Normal portrait, button on the bottom
	    default:
		history_chart.width = 320;
		history_chart.height = 150;
		break;
	}

	window.scrollTo(0,1);	// Try to hide the url bar.

	// Redraw the currently-displayed graph.
	current_graph();
}

/************************************************************************
 * Functions for calculating axis ticks.
 * From "Graphics gems", Andrew S. Glassner ed., Morgan Kaufmann, 1994,
 * p. 61.
 */
//var ntick = 5;			// How many tick marks we want

/* loose_label
 * Given the minimum and maximum data point, return an array of tick
 * marks at "nice" places.
 */
function loose_label(
	ntick,			// How many ticks we want
	data_min,		// Min. data value we're plotting
	data_max)		// Max. data value we're plotting
{
	// var nfrac;
	var d;			// Tick mark spacing
	var graphmin;		// Graph range min
	var graphmax;		// Graph range max
	var range;

	range = nicenum(data_max - data_min, false);
	d = nicenum(range/(ntick-1), true);
	graphmin = Math.floor(data_min/d)*d;
	graphmax = Math.ceil(data_max/d)*d;
	// nfrac = Math.max(-Math.floor(Math.log(d)/Math.LN10), 0);

	var retval = [];
	for (var x = graphmin; x <= graphmax+0.5*d; x += d)
	{
		retval.push(x);
	}
	return retval;
}

/* nicenum
 * Find a "nice" number approximately equal to x. Round the number if
 * 'round' is true, take ceiling if 'round' is false.
 */
function nicenum(x, round)
{
	var exp;		// Exponent of x
	var f;			// Fractional part of x
	var nf;			// Nice, rounded fraction

	exp = Math.floor(Math.log(x)/Math.LN10);
	f = x/Math.pow(10, exp);

	if (round)
	{
		if (f < 1.5)
			nf = 1;
		else if (f < 3.0)
			nf = 2;
		else if (f < 7.0)
			nf = 5;
		else
			nf = 10;
	} else {
		if (f <= 1.0)
			nf = 1;
		else if (f <= 2.0)
			nf = 2;
		else if (f <= 5.0)
			nf = 5;
		else
			nf = 10;
	}

	return nf * Math.pow(10, exp);
}

/************************************************************************
 * JavaScript canvas text functions.
 * From http://jim.studt.net/canvastext/canvastext.js
 */
//
// This code is released to the public domain by Jim Studt, 2007.
// He may keep some sort of up to date copy at http://www.federated.com/~jim/canvastext/
//
var CanvasTextFunctions = { };

CanvasTextFunctions.letters = {
    ' ': { width: 16, points: [] },
    '!': { width: 10, points: [[5,21],[5,7],[-1,-1],[5,2],[4,1],[5,0],[6,1],[5,2]] },
    '"': { width: 16, points: [[4,21],[4,14],[-1,-1],[12,21],[12,14]] },
    '#': { width: 21, points: [[11,25],[4,-7],[-1,-1],[17,25],[10,-7],[-1,-1],[4,12],[18,12],[-1,-1],[3,6],[17,6]] },
    '$': { width: 20, points: [[8,25],[8,-4],[-1,-1],[12,25],[12,-4],[-1,-1],[17,18],[15,20],[12,21],[8,21],[5,20],[3,18],[3,16],[4,14],[5,13],[7,12],[13,10],[15,9],[16,8],[17,6],[17,3],[15,1],[12,0],[8,0],[5,1],[3,3]] },
    '%': { width: 24, points: [[21,21],[3,0],[-1,-1],[8,21],[10,19],[10,17],[9,15],[7,14],[5,14],[3,16],[3,18],[4,20],[6,21],[8,21],[10,20],[13,19],[16,19],[19,20],[21,21],[-1,-1],[17,7],[15,6],[14,4],[14,2],[16,0],[18,0],[20,1],[21,3],[21,5],[19,7],[17,7]] },
    '&': { width: 26, points: [[23,12],[23,13],[22,14],[21,14],[20,13],[19,11],[17,6],[15,3],[13,1],[11,0],[7,0],[5,1],[4,2],[3,4],[3,6],[4,8],[5,9],[12,13],[13,14],[14,16],[14,18],[13,20],[11,21],[9,20],[8,18],[8,16],[9,13],[11,10],[16,3],[18,1],[20,0],[22,0],[23,1],[23,2]] },
    '\'': { width: 10, points: [[5,19],[4,20],[5,21],[6,20],[6,18],[5,16],[4,15]] },
    '(': { width: 14, points: [[11,25],[9,23],[7,20],[5,16],[4,11],[4,7],[5,2],[7,-2],[9,-5],[11,-7]] },
    ')': { width: 14, points: [[3,25],[5,23],[7,20],[9,16],[10,11],[10,7],[9,2],[7,-2],[5,-5],[3,-7]] },
    '*': { width: 16, points: [[8,21],[8,9],[-1,-1],[3,18],[13,12],[-1,-1],[13,18],[3,12]] },
    '+': { width: 26, points: [[13,18],[13,0],[-1,-1],[4,9],[22,9]] },
    ',': { width: 10, points: [[6,1],[5,0],[4,1],[5,2],[6,1],[6,-1],[5,-3],[4,-4]] },
    '-': { width: 26, points: [[4,9],[22,9]] },
    '.': { width: 10, points: [[5,2],[4,1],[5,0],[6,1],[5,2]] },
    '/': { width: 22, points: [[20,25],[2,-7]] },
    '0': { width: 20, points: [[9,21],[6,20],[4,17],[3,12],[3,9],[4,4],[6,1],[9,0],[11,0],[14,1],[16,4],[17,9],[17,12],[16,17],[14,20],[11,21],[9,21]] },
    '1': { width: 20, points: [[6,17],[8,18],[11,21],[11,0]] },
    '2': { width: 20, points: [[4,16],[4,17],[5,19],[6,20],[8,21],[12,21],[14,20],[15,19],[16,17],[16,15],[15,13],[13,10],[3,0],[17,0]] },
    '3': { width: 20, points: [[5,21],[16,21],[10,13],[13,13],[15,12],[16,11],[17,8],[17,6],[16,3],[14,1],[11,0],[8,0],[5,1],[4,2],[3,4]] },
    '4': { width: 20, points: [[13,21],[3,7],[18,7],[-1,-1],[13,21],[13,0]] },
    '5': { width: 20, points: [[15,21],[5,21],[4,12],[5,13],[8,14],[11,14],[14,13],[16,11],[17,8],[17,6],[16,3],[14,1],[11,0],[8,0],[5,1],[4,2],[3,4]] },
    '6': { width: 20, points: [[16,18],[15,20],[12,21],[10,21],[7,20],[5,17],[4,12],[4,7],[5,3],[7,1],[10,0],[11,0],[14,1],[16,3],[17,6],[17,7],[16,10],[14,12],[11,13],[10,13],[7,12],[5,10],[4,7]] },
    '7': { width: 20, points: [[17,21],[7,0],[-1,-1],[3,21],[17,21]] },
    '8': { width: 20, points: [[8,21],[5,20],[4,18],[4,16],[5,14],[7,13],[11,12],[14,11],[16,9],[17,7],[17,4],[16,2],[15,1],[12,0],[8,0],[5,1],[4,2],[3,4],[3,7],[4,9],[6,11],[9,12],[13,13],[15,14],[16,16],[16,18],[15,20],[12,21],[8,21]] },
    '9': { width: 20, points: [[16,14],[15,11],[13,9],[10,8],[9,8],[6,9],[4,11],[3,14],[3,15],[4,18],[6,20],[9,21],[10,21],[13,20],[15,18],[16,14],[16,9],[15,4],[13,1],[10,0],[8,0],[5,1],[4,3]] },
    ':': { width: 10, points: [[5,14],[4,13],[5,12],[6,13],[5,14],[-1,-1],[5,2],[4,1],[5,0],[6,1],[5,2]] },
// Duplicate entry
//    ',': { width: 10, points: [[5,14],[4,13],[5,12],[6,13],[5,14],[-1,-1],[6,1],[5,0],[4,1],[5,2],[6,1],[6,-1],[5,-3],[4,-4]] },
    '<': { width: 24, points: [[20,18],[4,9],[20,0]] },
    '=': { width: 26, points: [[4,12],[22,12],[-1,-1],[4,6],[22,6]] },
    '>': { width: 24, points: [[4,18],[20,9],[4,0]] },
    '?': { width: 18, points: [[3,16],[3,17],[4,19],[5,20],[7,21],[11,21],[13,20],[14,19],[15,17],[15,15],[14,13],[13,12],[9,10],[9,7],[-1,-1],[9,2],[8,1],[9,0],[10,1],[9,2]] },
    '@': { width: 27, points: [[18,13],[17,15],[15,16],[12,16],[10,15],[9,14],[8,11],[8,8],[9,6],[11,5],[14,5],[16,6],[17,8],[-1,-1],[12,16],[10,14],[9,11],[9,8],[10,6],[11,5],[-1,-1],[18,16],[17,8],[17,6],[19,5],[21,5],[23,7],[24,10],[24,12],[23,15],[22,17],[20,19],[18,20],[15,21],[12,21],[9,20],[7,19],[5,17],[4,15],[3,12],[3,9],[4,6],[5,4],[7,2],[9,1],[12,0],[15,0],[18,1],[20,2],[21,3],[-1,-1],[19,16],[18,8],[18,6],[19,5]] },
    'A': { width: 18, points: [[9,21],[1,0],[-1,-1],[9,21],[17,0],[-1,-1],[4,7],[14,7]] },
    'B': { width: 21, points: [[4,21],[4,0],[-1,-1],[4,21],[13,21],[16,20],[17,19],[18,17],[18,15],[17,13],[16,12],[13,11],[-1,-1],[4,11],[13,11],[16,10],[17,9],[18,7],[18,4],[17,2],[16,1],[13,0],[4,0]] },
    'C': { width: 21, points: [[18,16],[17,18],[15,20],[13,21],[9,21],[7,20],[5,18],[4,16],[3,13],[3,8],[4,5],[5,3],[7,1],[9,0],[13,0],[15,1],[17,3],[18,5]] },
    'D': { width: 21, points: [[4,21],[4,0],[-1,-1],[4,21],[11,21],[14,20],[16,18],[17,16],[18,13],[18,8],[17,5],[16,3],[14,1],[11,0],[4,0]] },
    'E': { width: 19, points: [[4,21],[4,0],[-1,-1],[4,21],[17,21],[-1,-1],[4,11],[12,11],[-1,-1],[4,0],[17,0]] },
    'F': { width: 18, points: [[4,21],[4,0],[-1,-1],[4,21],[17,21],[-1,-1],[4,11],[12,11]] },
    'G': { width: 21, points: [[18,16],[17,18],[15,20],[13,21],[9,21],[7,20],[5,18],[4,16],[3,13],[3,8],[4,5],[5,3],[7,1],[9,0],[13,0],[15,1],[17,3],[18,5],[18,8],[-1,-1],[13,8],[18,8]] },
    'H': { width: 22, points: [[4,21],[4,0],[-1,-1],[18,21],[18,0],[-1,-1],[4,11],[18,11]] },
    'I': { width: 8, points: [[4,21],[4,0]] },
    'J': { width: 16, points: [[12,21],[12,5],[11,2],[10,1],[8,0],[6,0],[4,1],[3,2],[2,5],[2,7]] },
    'K': { width: 21, points: [[4,21],[4,0],[-1,-1],[18,21],[4,7],[-1,-1],[9,12],[18,0]] },
    'L': { width: 17, points: [[4,21],[4,0],[-1,-1],[4,0],[16,0]] },
    'M': { width: 24, points: [[4,21],[4,0],[-1,-1],[4,21],[12,0],[-1,-1],[20,21],[12,0],[-1,-1],[20,21],[20,0]] },
    'N': { width: 22, points: [[4,21],[4,0],[-1,-1],[4,21],[18,0],[-1,-1],[18,21],[18,0]] },
    'O': { width: 22, points: [[9,21],[7,20],[5,18],[4,16],[3,13],[3,8],[4,5],[5,3],[7,1],[9,0],[13,0],[15,1],[17,3],[18,5],[19,8],[19,13],[18,16],[17,18],[15,20],[13,21],[9,21]] },
    'P': { width: 21, points: [[4,21],[4,0],[-1,-1],[4,21],[13,21],[16,20],[17,19],[18,17],[18,14],[17,12],[16,11],[13,10],[4,10]] },
    'Q': { width: 22, points: [[9,21],[7,20],[5,18],[4,16],[3,13],[3,8],[4,5],[5,3],[7,1],[9,0],[13,0],[15,1],[17,3],[18,5],[19,8],[19,13],[18,16],[17,18],[15,20],[13,21],[9,21],[-1,-1],[12,4],[18,-2]] },
    'R': { width: 21, points: [[4,21],[4,0],[-1,-1],[4,21],[13,21],[16,20],[17,19],[18,17],[18,15],[17,13],[16,12],[13,11],[4,11],[-1,-1],[11,11],[18,0]] },
    'S': { width: 20, points: [[17,18],[15,20],[12,21],[8,21],[5,20],[3,18],[3,16],[4,14],[5,13],[7,12],[13,10],[15,9],[16,8],[17,6],[17,3],[15,1],[12,0],[8,0],[5,1],[3,3]] },
    'T': { width: 16, points: [[8,21],[8,0],[-1,-1],[1,21],[15,21]] },
    'U': { width: 22, points: [[4,21],[4,6],[5,3],[7,1],[10,0],[12,0],[15,1],[17,3],[18,6],[18,21]] },
    'V': { width: 18, points: [[1,21],[9,0],[-1,-1],[17,21],[9,0]] },
    'W': { width: 24, points: [[2,21],[7,0],[-1,-1],[12,21],[7,0],[-1,-1],[12,21],[17,0],[-1,-1],[22,21],[17,0]] },
    'X': { width: 20, points: [[3,21],[17,0],[-1,-1],[17,21],[3,0]] },
    'Y': { width: 18, points: [[1,21],[9,11],[9,0],[-1,-1],[17,21],[9,11]] },
    'Z': { width: 20, points: [[17,21],[3,0],[-1,-1],[3,21],[17,21],[-1,-1],[3,0],[17,0]] },
    '[': { width: 14, points: [[4,25],[4,-7],[-1,-1],[5,25],[5,-7],[-1,-1],[4,25],[11,25],[-1,-1],[4,-7],[11,-7]] },
    '\\': { width: 14, points: [[0,21],[14,-3]] },
    ']': { width: 14, points: [[9,25],[9,-7],[-1,-1],[10,25],[10,-7],[-1,-1],[3,25],[10,25],[-1,-1],[3,-7],[10,-7]] },
    '^': { width: 16, points: [[6,15],[8,18],[10,15],[-1,-1],[3,12],[8,17],[13,12],[-1,-1],[8,17],[8,0]] },
    '_': { width: 16, points: [[0,-2],[16,-2]] },
    '`': { width: 10, points: [[6,21],[5,20],[4,18],[4,16],[5,15],[6,16],[5,17]] },
    'a': { width: 19, points: [[15,14],[15,0],[-1,-1],[15,11],[13,13],[11,14],[8,14],[6,13],[4,11],[3,8],[3,6],[4,3],[6,1],[8,0],[11,0],[13,1],[15,3]] },
    'b': { width: 19, points: [[4,21],[4,0],[-1,-1],[4,11],[6,13],[8,14],[11,14],[13,13],[15,11],[16,8],[16,6],[15,3],[13,1],[11,0],[8,0],[6,1],[4,3]] },
    'c': { width: 18, points: [[15,11],[13,13],[11,14],[8,14],[6,13],[4,11],[3,8],[3,6],[4,3],[6,1],[8,0],[11,0],[13,1],[15,3]] },
    'd': { width: 19, points: [[15,21],[15,0],[-1,-1],[15,11],[13,13],[11,14],[8,14],[6,13],[4,11],[3,8],[3,6],[4,3],[6,1],[8,0],[11,0],[13,1],[15,3]] },
    'e': { width: 18, points: [[3,8],[15,8],[15,10],[14,12],[13,13],[11,14],[8,14],[6,13],[4,11],[3,8],[3,6],[4,3],[6,1],[8,0],[11,0],[13,1],[15,3]] },
    'f': { width: 12, points: [[10,21],[8,21],[6,20],[5,17],[5,0],[-1,-1],[2,14],[9,14]] },
    'g': { width: 19, points: [[15,14],[15,-2],[14,-5],[13,-6],[11,-7],[8,-7],[6,-6],[-1,-1],[15,11],[13,13],[11,14],[8,14],[6,13],[4,11],[3,8],[3,6],[4,3],[6,1],[8,0],[11,0],[13,1],[15,3]] },
    'h': { width: 19, points: [[4,21],[4,0],[-1,-1],[4,10],[7,13],[9,14],[12,14],[14,13],[15,10],[15,0]] },
    'i': { width: 8, points: [[3,21],[4,20],[5,21],[4,22],[3,21],[-1,-1],[4,14],[4,0]] },
    'j': { width: 10, points: [[5,21],[6,20],[7,21],[6,22],[5,21],[-1,-1],[6,14],[6,-3],[5,-6],[3,-7],[1,-7]] },
    'k': { width: 17, points: [[4,21],[4,0],[-1,-1],[14,14],[4,4],[-1,-1],[8,8],[15,0]] },
    'l': { width: 8, points: [[4,21],[4,0]] },
    'm': { width: 30, points: [[4,14],[4,0],[-1,-1],[4,10],[7,13],[9,14],[12,14],[14,13],[15,10],[15,0],[-1,-1],[15,10],[18,13],[20,14],[23,14],[25,13],[26,10],[26,0]] },
    'n': { width: 19, points: [[4,14],[4,0],[-1,-1],[4,10],[7,13],[9,14],[12,14],[14,13],[15,10],[15,0]] },
    'o': { width: 19, points: [[8,14],[6,13],[4,11],[3,8],[3,6],[4,3],[6,1],[8,0],[11,0],[13,1],[15,3],[16,6],[16,8],[15,11],[13,13],[11,14],[8,14]] },
    'p': { width: 19, points: [[4,14],[4,-7],[-1,-1],[4,11],[6,13],[8,14],[11,14],[13,13],[15,11],[16,8],[16,6],[15,3],[13,1],[11,0],[8,0],[6,1],[4,3]] },
    'q': { width: 19, points: [[15,14],[15,-7],[-1,-1],[15,11],[13,13],[11,14],[8,14],[6,13],[4,11],[3,8],[3,6],[4,3],[6,1],[8,0],[11,0],[13,1],[15,3]] },
    'r': { width: 13, points: [[4,14],[4,0],[-1,-1],[4,8],[5,11],[7,13],[9,14],[12,14]] },
    's': { width: 17, points: [[14,11],[13,13],[10,14],[7,14],[4,13],[3,11],[4,9],[6,8],[11,7],[13,6],[14,4],[14,3],[13,1],[10,0],[7,0],[4,1],[3,3]] },
    't': { width: 12, points: [[5,21],[5,4],[6,1],[8,0],[10,0],[-1,-1],[2,14],[9,14]] },
    'u': { width: 19, points: [[4,14],[4,4],[5,1],[7,0],[10,0],[12,1],[15,4],[-1,-1],[15,14],[15,0]] },
    'v': { width: 16, points: [[2,14],[8,0],[-1,-1],[14,14],[8,0]] },
    'w': { width: 22, points: [[3,14],[7,0],[-1,-1],[11,14],[7,0],[-1,-1],[11,14],[15,0],[-1,-1],[19,14],[15,0]] },
    'x': { width: 17, points: [[3,14],[14,0],[-1,-1],[14,14],[3,0]] },
    'y': { width: 16, points: [[2,14],[8,0],[-1,-1],[14,14],[8,0],[6,-4],[4,-6],[2,-7],[1,-7]] },
    'z': { width: 17, points: [[14,14],[3,0],[-1,-1],[3,14],[14,14],[-1,-1],[3,0],[14,0]] },
    '{': { width: 14, points: [[9,25],[7,24],[6,23],[5,21],[5,19],[6,17],[7,16],[8,14],[8,12],[6,10],[-1,-1],[7,24],[6,22],[6,20],[7,18],[8,17],[9,15],[9,13],[8,11],[4,9],[8,7],[9,5],[9,3],[8,1],[7,0],[6,-2],[6,-4],[7,-6],[-1,-1],[6,8],[8,6],[8,4],[7,2],[6,1],[5,-1],[5,-3],[6,-5],[7,-6],[9,-7]] },
    '|': { width: 8, points: [[4,25],[4,-7]] },
    '}': { width: 14, points: [[5,25],[7,24],[8,23],[9,21],[9,19],[8,17],[7,16],[6,14],[6,12],[8,10],[-1,-1],[7,24],[8,22],[8,20],[7,18],[6,17],[5,15],[5,13],[6,11],[10,9],[6,7],[5,5],[5,3],[6,1],[7,0],[8,-2],[8,-4],[7,-6],[-1,-1],[8,8],[6,6],[6,4],[7,2],[8,1],[9,-1],[9,-3],[8,-5],[7,-6],[5,-7]] },
    '~': { width: 24, points: [[3,6],[3,8],[4,11],[6,12],[8,12],[10,11],[14,8],[16,7],[18,7],[20,8],[21,10],[-1,-1],[3,8],[4,10],[6,11],[8,11],[10,10],[14,7],[16,6],[18,6],[20,7],[21,10],[21,12]] }
};

CanvasTextFunctions.letter = function (ch)
{
    return CanvasTextFunctions.letters[ch];
}

CanvasTextFunctions.ascent = function( font, size)
{
    return size;
}

CanvasTextFunctions.descent = function( font, size)
{
    return 7.0*size/25.0;
}

CanvasTextFunctions.measure = function( font, size, str)
{
    var total = 0;
    var len = str.length;

    for ( i = 0; i < len; i++) {
	var c = CanvasTextFunctions.letter( str.charAt(i));
	if ( c) total += c.width * size / 25.0;
    }
    return total;
}

CanvasTextFunctions.draw = function(ctx,font,size,x,y,str)
{
    var total = 0;
    var len = str.length;
    var mag = size / 25.0;

    ctx.save();
    ctx.lineCap = "round";
    ctx.lineWidth = 2.0 * mag;

    for (var i = 0; i < len; i++) {
	var c = CanvasTextFunctions.letter( str.charAt(i));
	if ( !c) continue;

	ctx.beginPath();

	var penUp = 1;
	var needStroke = 0;
	for (var j = 0; j < c.points.length; j++) {
	    var a = c.points[j];
	    if ( a[0] == -1 && a[1] == -1) {
		penUp = 1;
		continue;
	    }
	    if ( penUp) {
		ctx.moveTo( x + a[0]*mag, y - a[1]*mag);
		penUp = false;
	    } else {
		ctx.lineTo( x + a[0]*mag, y - a[1]*mag);
	    }
	}
	ctx.stroke();
	x += c.width*mag;
    }
    ctx.restore();
    return total;
}

CanvasTextFunctions.enable = function( ctx)
{
    ctx.drawText = function(font,size,x,y,text) { return CanvasTextFunctions.draw( ctx, font,size,x,y,text); };
    ctx.measureText = function(font,size,text) { return CanvasTextFunctions.measure( font,size,text); };
    ctx.fontAscent = function(font,size) { return CanvasTextFunctions.ascent(font,size); }
    ctx.fontDescent = function(font,size) { return CanvasTextFunctions.descent(font,size); }

    ctx.drawTextRight = function(font,size,x,y,text) {
	var w = CanvasTextFunctions.measure(font,size,text);
	return CanvasTextFunctions.draw( ctx, font,size,x-w,y,text);
    };
    ctx.drawTextCenter = function(font,size,x,y,text) {
	var w = CanvasTextFunctions.measure(font,size,text);
	return CanvasTextFunctions.draw( ctx, font,size,x-w/2,y,text);
    };
}
