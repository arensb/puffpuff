<?php
$BACKUPFILE = strftime("backup/backup.%Y-%m");

header("Content-type: text/plain; charset=utf-8");

if (!isset($_POST['events']))
{
	header("HTTP/1.0 400 You suck");

	echo "events not defined.\n";
	exit(0);
}

if ($_POST['events'] == "")
	exit(0);

umask(002);	# Make file group-writable
$err = file_put_contents($BACKUPFILE, $_POST['events']."\n", FILE_APPEND);

if ($err === FALSE)
{
	header("HTTP/1.0 500 Something failed: ($BACKUPFILE) $err");
	echo "Backup not written for some reason.\n";
	exit(1);
}
echo "Successfully wrote $err bytes.\n";
?>
