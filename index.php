<?php
echo "<", '?xml version="1.0" encoding="UTF-8"?', ">\n";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html 
      xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      lang="en"
      manifest="puffpuff.manifest"
      >
<head>
<title>PuffPuff</title>
<link rel="stylesheet" type="text/css" href="puffpuff.css" media="all" />
<script type="text/javascript" src="puffpuff.js"></script>
<!-- Icon for standalone app -->
<link rel="apple-touch-icon" href="images/icon.png"/>
<!-- Splash screen for standalone app -->
<link rel="apple-touch-startup-image"
      href="images/iphone-splash.png"
      />
<meta name="viewport"
      content="width = device-width,
                initial-scale = 1.0,
                user-scalable = yes"/>
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="viewport"
      content="width = device-width,
                initial-scale = 1.0,
                user-scalable = no"/>
</head>
<body>

<div id="pages">
<!-- Main page -->
<div class="page" id="main-page">
<button id="smoke" value=""/>
</div>

<div class="page" id="graph-page">
<canvas id="history-chart" width="320" height="150"></canvas>

<table class="graph-button-bar">
<tr>
  <td><a id="show-day">24h</a></td>
  <td><a id="show-week">7d</a></td>
  <td><a id="show-30d">30d</a></td>
  <td><a id="show-12m">12m</a></td>
</tr>
</table>

</div>

<!-- Editing page
     Allow user to edit history.
-->
<div class="page" id="edit-page">
<p>(Editing page)</p>
<ul>
<li><button class="pseudo-button" onclick="localStorage.clear()">Clear localStorage</button></li>
</ul>
</div>

<!-- Settings page -->
<div class="page" id="conf-page">
<p>(Settings page)</p>

<ul>
<li><button class="pseudo-button" onclick="hist.backup(true)">Backup</button></li>
<li><button class="pseudo-button" onclick="hist.restore()">Restore</button></li>
</div>
</div><!-- pages -->

<table id="page-button-bar">
<tr>
<td><a id="show-page-main">Main</a></td>
<td><a id="show-page-graph">Graph</a></td>
<td><a id="show-page-edit">Edit</a></td>
<td><a id="show-page-conf">Conf</a></td>
</tr>
</table>

</body>
</html>
