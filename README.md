# Puffpuff
A smoking tracker.

Just tap the icon when you light up, and it'll record as having smoked. It'll show you a graph of how much you've smoked in the last day, week, month, and year.

This was written for personal use, so it's rather rough around the edges.

## Local application
You should be able to download this to your phone's desktop. In Firefox, use "Save to Desktop" under the menu.

## Credits
images/Papierosa_1_ubt_0069.jpeg
	Tomasz Sienicki, http://commons.wikimedia.org/wiki/File:Papierosa_1_ubt_0069.jpeg
	GNU Free Documentation License

images/Aiga_smoking_inv.svg 
	Pluke, http://commons.wikimedia.org/wiki/File:Aiga_smoking_inv.svg
	Public domain
